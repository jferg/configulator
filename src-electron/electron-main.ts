import { app, BrowserWindow, Menu, Tray, nativeTheme } from 'electron';
import path from 'path';

import { initialize } from '@electron/remote/main';

let isQuitting = false;

try {
  if (process.platform === 'win32' && nativeTheme.shouldUseDarkColors) {
    // eslint-disable-next-line
    require('fs').unlinkSync(require('path').join(app.getPath('userData'), 'DevTools Extensions'))
  }
} catch (_) { }

initialize();

let mainWindow: BrowserWindow | null;

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 300,
    width: 600,
    maxHeight: 300,
    maxWidth: 600,
    minHeight: 300,
    minWidth: 600,
    useContentSize: true,
    frame: false,
    skipTaskbar: true,
    webPreferences: {
      contextIsolation: true,
      preload: path.resolve(__dirname, process.env.QUASAR_ELECTRON_PRELOAD ?? ''),
      enableRemoteModule: true,
    },
    alwaysOnTop: true,
  });
  const iconFile: string = require('path').resolve(__dirname, 'icons', 'trayicon.ico');
  console.log(iconFile);
  let tray: Tray;
  try {
    tray = new Tray(iconFile);
  } catch (e) {
    console.log( 'betrayed', e);
  }

  const contextMenu = Menu.buildFromTemplate([
    { label: 'Show Configulator', type: 'radio', click: function() {
     mainWindow?.show();
      } },
    { label: 'Exit', click: function() {
      isQuitting = true;
      app.quit();
      } },
  ]);
  // tray.setToolTip('Configulator');
  // tray.setContextMenu(contextMenu);

  mainWindow.setAlwaysOnTop(true, 'floating');

  void mainWindow.loadURL(process.env.APP_URL ?? '')

  if (process.env.DEBUGGING) {
    // if on DEV or Production with debug enabled
    mainWindow.webContents.openDevTools()
  } else {
    // we're on production; no access to devtools pls
    mainWindow.webContents.on('devtools-opened', () => {
      mainWindow?.webContents.closeDevTools()
    })
  }

  mainWindow.on('closed', (event: Event) => {
    if (!isQuitting) {
      event.preventDefault();
      mainWindow?.hide();
      event.returnValue = false;
    } else {
      mainWindow = null
    }
  })
}

app.on('ready', () => {
  createWindow();
});

app.on('before-quit', function() {
  isQuitting = true;
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
})
